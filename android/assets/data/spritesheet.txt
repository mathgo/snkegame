spritesheet.png
format: RGBA8888
filter: Linear,Linear
repeat: none
grass_tile
  rotate: false
  xy: 2, 2
  size: 512, 512
  orig: 512, 512
  offset: 0, 0
  index: -1
snake_head
  rotate: false
  xy: 2, 516
  size: 50, 50
  orig: 50, 50
  offset: 0, 0
  index: -1
