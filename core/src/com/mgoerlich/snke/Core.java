package com.mgoerlich.snke;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.mgoerlich.snke.screens.GameScreen;
import com.mgoerlich.snke.utils.AssetLoader;

public class Core extends Game {

    @Override
    public void create() {
        AssetLoader.load(new TextureAtlas("data/spritesheet.txt"));
        setScreen(new GameScreen());
    }

    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }

}
