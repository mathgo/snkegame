package com.mgoerlich.snke.utils;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

import java.util.HashMap;

public class AssetLoader {
    public static TextureAtlas atlas;
    private static Sprite background;

    private static HashMap<String, Sprite> snake_sprites = new HashMap<String, Sprite>();

    public static void load(TextureAtlas atlas) {
        AssetLoader.atlas = atlas;
    }

    public static void dispose() {
        atlas.dispose();
    }

    public static Sprite getBackground() {
        if (null == background) {
            background = atlas.createSprite("grass_tile");
        }

        return background;
    }

    public static Sprite getSnakeSprite(String sprite_name) {
        if (!snake_sprites.containsKey(sprite_name)) {
            snake_sprites.put(sprite_name, atlas.createSprite("snake_" + sprite_name));
        }

        return snake_sprites.get(sprite_name);
    }
}
