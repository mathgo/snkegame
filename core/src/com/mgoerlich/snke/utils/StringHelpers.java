package com.mgoerlich.snke.utils;

import java.util.Arrays;

/**
 * StringHelpers is (or hopefully will get) a collection of useful helpers
 * for some common String operations.
 *
 * @author Manuel Goerlich
 */
public class StringHelpers {

    /**
     * Repeats a given character a given amount of times and
     * return all of them as a String.
     * <p/>
     * TODO: Put this somewhere more useful
     *
     * @param chr
     *         The char that should be repeated.
     * @param repetition_count
     *         How often should it be repeated.
     *
     * @return String containing {@code repetition_count} times {@code chr}.
     */
    public static String repeatCharAsString(char chr, int repetition_count) {
        char[] chars = new char[repetition_count];
        Arrays.fill(chars, chr);
        return new String(chars);
    }
}
