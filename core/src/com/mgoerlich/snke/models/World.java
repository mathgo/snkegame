package com.mgoerlich.snke.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mgoerlich.snke.utils.AssetLoader;

public class World {
    private Snake snake = new Snake();
    private Sprite background = AssetLoader.getBackground();

    private OrthographicCamera camera;
    private Viewport viewport;

    private float width, height, aspectRatio;
    private boolean isTouch;


    public World(float width, float height) {
        this.width = width;
        this.height = height;
        aspectRatio = ((float) Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight());
        camera = new OrthographicCamera(height * aspectRatio, height);
        viewport = new FitViewport(width, height, camera);

        background.setSize(width, height);
        camera.translate(width / 2, height / 2);
        viewport.apply();
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void update(float delta) {
        camera.update();
    }

    public void draw(Batch batch) {
        if (!batch.isDrawing()) {
            batch.begin();
        }

        background.draw(batch);

        if (isTouch) {
            snake.draw(batch);
        }

        if (batch.isDrawing()) {
            batch.end();
        }
    }

    public boolean onPan(Vector2 pos, Vector2 delta) {
        if (!isTouch) {
            isTouch = true;
        }

        snake.moveTo(getWorldCoordinates(pos), delta);
        return true;
    }

    public boolean onPanEnd() {
        isTouch = false;
        return true;
    }

    private Vector2 getWorldCoordinates(Vector2 coords) {
        return viewport.unproject(coords);
    }

    private Vector2 getWorldCoordinates(float x, float y) {
        return getWorldCoordinates(new Vector2(x, y));
    }
}
