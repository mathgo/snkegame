package com.mgoerlich.snke.models;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.mgoerlich.snke.utils.AssetLoader;

public class Snake extends BaseDrawable {
    private Sprite sprite;
    private Vector2 position = new Vector2(0, 0);

    public Snake() {
        sprite = AssetLoader.getSnakeSprite("head");
    }

    public void draw(Batch batch) {
        if (!batch.isDrawing()) {
            batch.begin();
        }

        sprite.draw(batch);
    }

    public void moveTo(Vector2 coords, Vector2 delta) {
        float degrees = (float) Math.atan2(delta.y, -delta.x);
        float rotation = (float) (degrees * 180.0d / Math.PI) + 90.f;
        setRotation(rotation);
        setCenter(coords);
    }

    public void setCenter(Vector2 coordinates) {
        sprite.setCenter(coordinates.x, coordinates.y);
    }

    public void setRotation(float degrees) {
        sprite.setRotation(degrees);
    }
}
