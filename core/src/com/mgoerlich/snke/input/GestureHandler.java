package com.mgoerlich.snke.input;

import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.mgoerlich.snke.models.World;

public class GestureHandler implements GestureDetector.GestureListener {
    private World world;

    private Vector2 position = new Vector2();
    private Vector2 positionDelta = new Vector2();

    public GestureHandler(World world) {
        this.world = world;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        position.set(x, y);
        positionDelta.set(deltaX, deltaY);
        return world.onPan(position, positionDelta);
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return world.onPanEnd();
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1,
                         Vector2 pointer2) {
        return false;
    }
}
