package com.mgoerlich.snke.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.mgoerlich.snke.input.GestureHandler;

public class GameScreen implements Screen {
    private com.mgoerlich.snke.models.World world;

    private SpriteBatch batch = new SpriteBatch();

    final float GAME_WIDTH = 1024;
    final float GAME_HEIGHT = 600;

    GestureDetector gestureDetector;

    public GameScreen() {
        world = new com.mgoerlich.snke.models.World(GAME_WIDTH, GAME_HEIGHT);
        gestureDetector = new GestureDetector(new GestureHandler(world));

        batch.setProjectionMatrix(world.getViewport().getCamera().combined);
        Gdx.input.setInputProcessor(gestureDetector);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.update(delta);
        world.draw(batch);
    }

    @Override
    public void resize(int width, int height) {
        world.getViewport().update(width, height);
    }

    @Override
    public void dispose() {
        batch.dispose();
    }

    @Override
    public void show() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }
}
